<?php

namespace Drupal\cg_payment\Manager;

use Creditguard\CgCommandRequestChargeToken;
use Creditguard\CgCommandRequestPaymentFormUrl;
use Drupal\cg_payment\CgChargeEvent;
use Drupal\cg_payment\Entity\Transaction;
use Drupal\cg_payment\RequestInterface;
use Drupal\cg_payment\TransactionInterface;
use Drupal\cg_payment\Utility\TransactionTrait;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Exception;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * Class RequestManager.
 *
 * @package Drupal\cg_payment\Manager
 */
class RequestManager implements RequestInterface {

  use StringTranslationTrait;
  use TransactionTrait;

  protected $config;
  protected $successUrl;
  protected $cancelUrl;
  protected $errorUrl;
  protected $transaction;

  /**
   * The PrivateTempStoreFactory object.
   *
   * @var PrivateTempStoreFactory
   */
  protected $privateTempStore;

  /**
   * The EventDispatcherInterface object.
   *
   * @var EventDispatcherInterface
   */
  protected $dispatcher;

  /**
   * The MessengerInterface object.
   *
   * @var MessengerInterface
   */
  protected $messenger;

  /**
   * The RequestManager logging channel.
   *
   * @var LoggerChannelInterface
   */
  protected $logger;

  /**
   * The current request.
   *
   * @var Request
   */
  protected $request;

  /**
   * Language Manager service reference.
   * @var LanguageManagerInterface
   */
  protected $language_manager;

  /**
   * The error code after the payment.
   * @var String
   */
  protected $errorCode;

  /**
   * The error message after the payment.
   * @var String
   */
  protected $errorMessage;

  /**
   * RequestManager constructor.
   *
   * @param ConfigFactory $config
   *   The ConfigFactory object.
   * @param PrivateTempStoreFactory $privateTempStore
   *   The PrivateTempStoreFactory object.
   * @param EventDispatcherInterface $dispatcher
   *   The EventDispatcherInterface object.
   * @param MessengerInterface $messenger
   *   The Messenger object.
   * @param LoggerChannelFactoryInterface $channelFactory
   *   The LoggerChannelFactoryInterface object.
   * @param RequestStack $request_stack
   *   The request stack.
   * @param LanguageManagerInterface $language_manager
   */
  public function __construct(ConfigFactory $config,
                              PrivateTempStoreFactory $privateTempStore,
                              EventDispatcherInterface $dispatcher,
                              MessengerInterface $messenger,
                              LoggerChannelFactoryInterface $channelFactory,
                              RequestStack $request_stack,
                              LanguageManagerInterface $language_manager) {
    $this->config = $config;
    $this->privateTempStore = $privateTempStore;
    $this->dispatcher = $dispatcher;
    $this->messenger = $messenger;
    $this->logger = $channelFactory->get('cg_payment');
    $this->request = $request_stack->getCurrentRequest();
    $this->language_manager = $language_manager;

    // Get the current language code.
    $language = $this->language_manager->getCurrentLanguage();
    // Get return URL.
    $return_url = $return_url = Url::fromRoute('cg_payment.payment_complete', [], ['absolute' => TRUE, 'language' => $language])->toString();
    // Url to redirect user after successful payment.
    $this->successUrl = $return_url;
    // Url to redirect user in case he cancel.
    $this->cancelUrl = $return_url;
    // Url to redirect user in case of payment failure.
    $this->errorUrl = $return_url;
    // Set error code and message to NULL.
    $this->errorCode = '';
    $this->errorMessage = '';
  }

  /**
   * {@inheritdoc}
   */
  public function requestChargeToken($txId, $token, $cardExp, $amount = NULL, $personalId = '') {
    $transaction = $this->getTransactionByRemoteId($txId);

    // Quit if no transaction can be found with that txId.
    if (empty($transaction)) {
      return FALSE;
    }

    // Get credentials from our configuration.
    // @todo Move config to another service.
    $cg_config = $this->config->get('cg_payment.settings');
    $relayUrl = $cg_config->get('endpoint_url');
    $username = $cg_config->get('user_name');
    $terminalNumber = $cg_config->get('terminal_id');
    $mid = $cg_config->get('mid');
    $password = $cg_config->get('password');
    // Get the version, default to 100 as it was before this option was added.
    $version = $cg_config->get('version') ?: '100';

    // Try getting the charge data from our session storage (saved on the
    // requestPaymentFormUrl method) in case the function haven't receive
    // those details.
    $tempstore = $this->privateTempStore->get('cg_payment');
    $amount = !empty($amount) ? $amount : $tempstore->get('amount');

    if (empty($terminalNumber) || empty($mid) || empty($amount)) {
      // Log the bad call to watchdog.
      $this->logger->notice(
        'Trying to charge token, but some data is missing: TxID: @txnid, terminal id: @terminal_id, mid: @mid, amount: @amount', [
        '@txnid' => $txId,
        '@terminal_id' => $terminalNumber ?: '',
        '@mid' => $mid ?: '',
        '@amount' => $amount ?: '',
      ]);

      return FALSE;
    }

    try {
      $request = new CgCommandRequestChargeToken($relayUrl, $username, $password, $terminalNumber, $mid);
      $request
        ->setVersion($version)
        ->setTotal($amount)
        ->setCardToken($token)
        ->setCardExp($cardExp)
        ->setTxId($txId)
        ->setId($personalId);

      // Dispatch event to allow other modules interact with the data before
      // making the actual request to CG (e.g. for adding extra data).
      $event = new CgChargeEvent($request, $transaction);
      $this->dispatcher->dispatch(CgChargeEvent::PRE_CHARGE, $event);

      /* @var CgCommandRequestChargeToken $charge_response */
      $charge_response = $request->execute();
      $is_success = $charge_response->isSuccessCode();

      $authNumber = !empty($charge_response->getResponse()->doDeal->authNumber) ? reset($charge_response->getResponse()->doDeal->authNumber) : '-1';
      $transaction->set('expiration_date', $cardExp)
        ->set('confirm_num', $authNumber)
        ->set('status', $is_success ? 'success' : 'failure')
        ->save();

      // Log success and error responses.
      if ($is_success) {
        $this->logger->info(
          'CG charge token request completed: TxID: @txnid, internal transaction ID: @tid to terminal: @terminalNumber', [
          '@txnid' => $txId,
          '@tid' => $transaction->id(),
          '@terminalNumber' => $terminalNumber
        ]);
      }
      else {
        $error_message = !empty((string) $charge_response->getResponse()->doDeal->statusText) ?
          (string) $charge_response->getResponse()->doDeal->statusText : '';
        $this->errorCode = !empty($charge_response->getResponse()->result) ? $charge_response->getResponse()->result : FALSE;
        $this->errorMessage = $error_message;
        $this->logger->warning(
          'CG charge token request completed with a failure: TxID: @txnid, internal transaction ID: @tid, CreditGuard message: @message', [
          '@txnid' => $txId,
          '@tid' => $transaction->id(),
          '@message' => $error_message,
        ]);
      }

      return $is_success ? $transaction : FALSE;
    }
    catch (Exception $e) {
      $this->errorCode = $e->getCode();
      $this->errorMessage = $e->getMessage();
      $this->logger->error(
        'CG charge token request failed: TxID: @txnid, error code: @code, error message: @message', [
        '@txnid' => $txId,
        '@code' => $e->getCode(),
        '@message' => $e->getMessage(),
      ]);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function requestPaymentFormUrl($amount, $email, $description, $terminalNumber = NULL, $mid = NULL, $cgLangcode = 'HEB') {

    $url = NULL;
    $this->transaction = Transaction::create();

    // Get credentials from our configuration.
    $cg_config = $this->config->get('cg_payment.settings');
    $relayUrl = $cg_config->get('endpoint_url');
    $username = $cg_config->get('user_name');
    $terminalNumber = $terminalNumber ?? $cg_config->get('terminal_id');
    $mid = $mid ?? $cg_config->get('mid');
    $password = $cg_config->get('password');
    // Get the version, default to 100 as it was before this option was added.
    $version = $cg_config->get('version') ?: '100';

    try {
      $request = new CgCommandRequestPaymentFormUrl($relayUrl, $username, $password, $terminalNumber, $mid);
      $request
        ->setVersion($version)
        ->setTotal($amount)
        ->setEmail($email)
        ->setDescription($description)
        ->setSuccessUrl($this->successUrl)
        ->setCancelUrl($this->cancelUrl)
        ->setErrorUrl($this->errorUrl)
        ->setLanguage($cgLangcode);

      /* @var  CgCommandRequestPaymentFormUrl $cg_response */
      $cg_response = $request->execute();

      // If the transaction was not successful, log the result for debugging purposes.
      $result = $cg_response->getDoDealResult();
      if (isset($result['resultCode']) && $result['resultCode'] != '000') {
        // @todo Consider throwing an exception here so it can be handled by the caller.
        $this->logger->debug(print_r($cg_response->getDoDealResult(), TRUE));
      }

      $redirect_url = $cg_response->getPaymentFormUrl();
      $token_id = $cg_response->getPaymentFormToken();

      $this->transaction->set('status', 'pending');
      $this->transaction->set('remote_id', $token_id);

      $url = $redirect_url;

      $this->transaction->save();

      // Save data for later usage (e.g. the actual token charge).
      $tempstore = $this->privateTempStore->get('cg_payment');
      $tempstore->set('amount', $amount);

    }
    catch (Exception $e) {
      $this->transaction
        ->set('status', 'failure');
      $this->messenger->addError(
        $this->t('An error has occurred, please try again.')
      );
      print $e->getMessage();
    }

    return $url;
  }

  /**
   * Set the success URL.
   *
   * @param string $successUrl
   *   The success URL.
   *
   * @return RequestManager
   *   The request manager object.
   */
  public function setSuccessUrl($successUrl) {
    $this->successUrl = $successUrl;
    return $this;
  }

  /**
   * Set the cancel URL.
   *
   * @param string $cancelUrl
   *   The cancel URL.
   *
   * @return RequestManager
   *   The request manager object.
   */
  public function setCancelUrl($cancelUrl) {
    $this->cancelUrl = $cancelUrl;
    return $this;
  }

  /**
   * Set the error URL.
   *
   * @param string $errorUrl
   *   The error URL.
   *
   * @return RequestManager
   *   The request manager object.
   */
  public function setErrorUrl($errorUrl) {
    $this->errorUrl = $errorUrl;
    return $this;
  }

  /**
   * Get the transaction object.
   *
   * @return TransactionInterface
   *   The transaction entity.
   */
  public function getTransaction() {
    return $this->transaction;
  }

  /**
   * Return the error code.
   * @return String
   */
  public function getErrorCode() {
    return $this->errorCode;
  }

  /**
   * Return the erroe message.
   * @return String
   */
  public function getErrorMessage() {
    return $this->errorMessage;
  }

}
