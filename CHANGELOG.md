CreditGuard Payment 1.0, Beta 4 (2019-06-06):
-------------------
Advanced beta release of the project's Drupal 8 version. The API can be considered
mostly stable and an upgrade path will be provided for all data structure
changes from this point forward.

**Changes since 1.0 beta 3**
- API changes to `RequestInterface::requestPaymentFormUrl` and `RequestInterface::requestChargeToken`, terminal id and merchant id now
fetched from the module's configuration and no longer passed as arguments to the methods.  